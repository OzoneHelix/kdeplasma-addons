# kdeplasma-addons: 
- Based on the work of KDE developers: Link to original source code: [kdeplasma-addons](https://invent.kde.org/plasma/kdeplasma-addons)
- Purpose of this repo is to preserve the System Load Viewer widget that got removed from [KDE Plasma](https://invent.kde.org/plasma/kdeplasma-addons) as of version 5.21.
- To serve as a space for discussions and improvements from the community that could land in future versions of the widget.

## Content:
- The main branch contains just the systemloadviewer applet. It is a CPU/RAM/SWAP monitor applet that you can put on the Panel/Taskmanager/Desktop to always have a look at the system resources usage.
- Other addons/applets can be found in the original Plasma branch.

## Insall:
- From within Plasma itself (recommended).
    - Right click the taskmanager/panel or desktop and click "Add Widgets"
    - Then click "Get New Widgets", search for "System Load Viewer" and install it.
    - Again right click the taskmanager/panel or desktop, Add Widgets and double click this time the new installed one from the list.

- From [the KDE store website](https://store.kde.org/p/1474921)
- If you prefer the terminal, run the following commands:

````sh
git clone --branch main --single-branch https://gitlab.com/hkanjal/kdeplasma-addons.git
cd kdeplasma-addons
chmod u+x install.sh
./install.sh
````
